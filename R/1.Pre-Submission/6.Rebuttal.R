# Author:  Job van Riet
# Date:   24-11-2020
# Function: Performing additional tasks / re-checks as asked by the reviewers.


# Load libraries and data -------------------------------------------------

load('~/Downloads/samples.meta.RData')
load('~/Downloads/data.NeuroEndocrineCohort.Rdata')

data.Cohort <- data.Cohort[names(data.Cohort) %in% samples.meta[samples.meta$primaryTumorLocation == 'NEC',]$sampleId]

# Perform mutational signature analysis mNEC/mNET -------------------------

# Retrieve the mutations.
dataMuts <- base::lapply(data.Cohort, function(x){

    x <- x$somaticVariants
    muts <- x
    S4Vectors::mcols(muts) <- NULL
    muts$set <- x$set
    muts$ALT <- x$ALT
    muts$REF <- x$REF

    return(list(
        SNV = muts[muts$set == 'snvs',],
        InDels = muts[muts$set == 'indels',],
        MNV = muts[muts$set == 'mnvs',])
    )

})

names(dataMuts) <- names(data.Cohort)

dataMuts.Combined <- list()
dataMuts.Combined$SNV <- GenomicRanges::GRangesList(lapply(dataMuts, function(x) MutationalPatterns::get_mut_type(x$SNV, type = 'snv')))
dataMuts.Combined$InDels <- GenomicRanges::GRangesList(lapply(dataMuts, function(x) MutationalPatterns::get_mut_type(x$InDels, type = 'indel')))
dataMuts.Combined$DBS <- GenomicRanges::GRangesList(lapply(dataMuts, function(x) x$MNV[nchar(x$MNV$REF) == 2 & unlist(nchar(x$MNV$ALT)) == 2,]))

rm(data.Cohort)
rm(dataMuts)
gc()

# Retrieve the COSMIC v3.1 signatures -------------------------------------

sprintf('\tRetrieving COSMIC (v3.1) signature matrices.') %>% ParallelLogger::logInfo()

mutSigs.COSMIC <- list()
mutSigs.COSMIC$SNV <- MutationalPatterns::get_known_signatures(muttype = 'snv', source = 'COSMIC', sig_type = 'reference', incl_poss_artifacts = F)
mutSigs.COSMIC$InDel <- MutationalPatterns::get_known_signatures(muttype = 'indel', source = 'COSMIC', sig_type = 'reference', incl_poss_artifacts = F)
mutSigs.COSMIC$DBS <- MutationalPatterns::get_known_signatures(muttype = 'dbs', source = 'COSMIC', sig_type = 'reference', incl_poss_artifacts = F)


# Retrieve mutational motifs ----------------------------------------------

sprintf('\tConverting GRangesLists into mutational matrices.') %>% ParallelLogger::logTrace()

data.mutMatrix <- list()

# SNV (i.e. SBS)
data.mutMatrix$SNV <- MutationalPatterns::mut_matrix(dataMuts.Combined$SNV, ref_genome =  'BSgenome.Hsapiens.UCSC.hg19')

# InDel
data.mutMatrix$InDel <- MutationalPatterns::get_indel_context(dataMuts.Combined$InDel, ref_genome =  'BSgenome.Hsapiens.UCSC.hg19')
data.mutMatrix$InDel <- MutationalPatterns::count_indel_contexts(data.mutMatrix$InDel)

# DBS
data.mutMatrix$DBS <- MutationalPatterns::get_dbs_context(dataMuts.Combined$DBS)
data.mutMatrix$DBS <- MutationalPatterns::count_dbs_contexts(data.mutMatrix$DBS)


# Perform mutational signature fitting ------------------------------------

sprintf('\tPerforming mutational signature fitting using %s.', ifelse(restrictiveFit, 'restrictive fitting', 'regular fitting method')) %>% ParallelLogger::logInfo()

data.FittedMuts <- list()

data.FittedMuts$SNV <- MutationalPatterns::fit_to_signatures_strict(data.mutMatrix$SNV, mutSigs.COSMIC$SNV, max_delta = 0.01)
data.FittedMuts$InDel <- MutationalPatterns::fit_to_signatures_strict(data.mutMatrix$InDel, mutSigs.COSMIC$InDel, max_delta = 0.01)
data.FittedMuts$DBS <- MutationalPatterns::fit_to_signatures_strict(data.mutMatrix$DBS, mutSigs.COSMIC$DBS, max_delta = 0.01)

data.FittedMuts$SNV$sim_decay_fig <- NULL
data.FittedMuts$InDel$sim_decay_fig <- NULL
data.FittedMuts$DBS$sim_decay_fig <- NULL


# Determine recurrent non-coding aberrations ------------------------------


# Determine gene-overlapping SV -------------------------------------------
