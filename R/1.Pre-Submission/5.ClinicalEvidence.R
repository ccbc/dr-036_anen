# Author:  Job van Riet
# Date:   06-05-20
# Function: This script generates the clinical evidence figure for the CPCT-02 NET cohort manuscript.

# Load libraries ----------------------------------------------------------

pacman::p_load('R2CCBC', 'plyr', 'dplyr', 'tidyr', 'ggplot2')

load('/mnt/data2/hartwig/DR36/v2/RData/samples.meta.RData')
load('/mnt/data2/hartwig/DR36/v2/RData/data.Cohort.mutLoad.RData')


# Helper functions --------------------------------------------------------

annotationTheme <- function(){
    theme(legend.position = 'bottom', axis.ticks = element_blank(), axis.title.y = element_text(size = 8), axis.text.x = element_blank(), text=element_text(size=8, family='Helvetica'),
          panel.background = element_rect(fill = NA),
          panel.grid.major = element_line(NULL),
          plot.margin = ggplot2::margin(t = 0, 0,0,0, 'cm'),
          legend.margin = ggplot2::margin(t=-1, r=0, b=.5, l=0, unit='cm')
    )
}


# Read clinical evidence results ------------------------------------------

data <- readxl::read_xlsx('/mnt/data2/hartwig/DR36/v2/metadata/iClusionAndTargets.xlsx', trim_ws = T)

# Remove non-inclusion samples.
data <- data %>% dplyr::filter(patientId %in% samples.meta$patientId) %>% dplyr::mutate(gene = gsub(' .*', '', event))
data <- data %>% dplyr::mutate(subGroupMajor = ifelse(primaryTumorLocation == 'NEC', 'mNEC', paste0('mNET - ', subGroupSummarized)))
data <- data %>% dplyr::mutate(gene = ifelse(gene == 'Microsatellite', 'TMB ≥ 10', gene))


# Generate figure(s) ------------------------------------------------------

# Total patients per label
plotA <- data %>%
    dplyr::filter(grepl('Yes', onLabelNEN) | grepl('Yes', onLabelNENSubtype) | grepl('Yes', offLabelClinicalPractice) | grepl('Yes|No', offLabelAvailable)) %>%
    dplyr::filter(response == 'Responsive') %>%
    dplyr::group_by(hmfId) %>%
    # Determine optimal label
    dplyr::summarise(
        labelType = ifelse(any(grepl('Yes', onLabelNENSubtype)), 'On-label - NEN-subtype',
                           ifelse(any(grepl('Yes', onLabelNEN)), 'On-label - NEN',
                                  ifelse(any(grepl('Yes', offLabelClinicalPractice)), 'Off-label - NEN',
                                         ifelse(any(grepl('Yes', offLabelAvailable)), 'Off-label -  Other Cancer Types',
                                                ifelse(any(grepl('No', offLabelAvailable)), 'Drugs In Development', NA)))))
    ) %>%
    dplyr::group_by(hmfId) %>% dplyr::summarise(labelType = ifelse(any(labelType %in% 'On-label - NEN-subtype'), 'On-label - NEN-subtype', ifelse(any(labelType %in% 'On-label - NEN'), 'On-label - NEN', ifelse(any(labelType %in% 'Off-label - NEN'), 'Off-label - NEN', ifelse(any(labelType %in% 'Off-label -  Other Cancer Types'), 'Off-label -  Other Cancer Types', 'Drugs In Development'))))) %>%
    dplyr::mutate(labelType = factor(labelType, levels = c('On-label - NEN-subtype', 'On-label - NEN', 'Off-label - NEN', 'Off-label -  Other Cancer Types', 'Drugs In Development'))) %>%
    ggplot(aes(x = labelType, fill = labelType)) +
    geom_bar(color = 'black' ) +
    scale_y_continuous(expand = expand_scale(mult = c(0, .05)), limits = c(0, 16)) +
    geom_text(aes(label = sprintf('%s\n(%s%%)', ..count.., round(..count.. / 86 * 100, 0)), y= ..count..), stat= 'count',  vjust = -.5, size = 2.5) +
    scale_fill_manual(values = c('On-label - NEN-subtype' = '#55C23B', 'On-label - NEN' = '#1F92AE', 'Off-label - NEN' = '#DC3F65', 'Off-label -  Other Cancer Types' = '#F49700', 'Drugs In Development' = '#9BEBEE'), guide = guide_legend(title = 'Types', title.position = 'top', title.hjust = 0.5, nrow = 1, keywidth = 0.75, keyheight = 0.75)) +
    labs(x = 'Types', y = '# mNEN patients') +
    theme(
        text = element_text(family = 'Helvetica'),
        legend.position = 'bottom',
        panel.background = element_rect(fill = 'white', colour = 'white'),
        axis.title = element_text(face = 'bold',size = rel(1)),
        axis.title.y = element_text(angle=90,vjust =2),
        axis.title.x = element_text(vjust = -0.2),
        axis.text.x = element_text(angle = 45, hjust = 1),
        axis.line = element_line(colour='black'),
        panel.grid.major.x = element_blank(),
        panel.grid.minor.x = element_blank(),
        panel.grid.major.y = element_line(colour = 'grey20', linetype = 'dotted'),
        panel.grid.minor.y = element_line(colour = 'grey50', linetype = 'dotted')
    )

# Overview per sample with actionable alterations.
heatData <- data %>%
    dplyr::filter(grepl('Yes', onLabelNEN) | grepl('Yes', onLabelNENSubtype) | grepl('Yes', offLabelClinicalPractice) | grepl('Yes|No', offLabelAvailable)) %>%
    dplyr::filter(response == 'Responsive') %>%
    dplyr::group_by(hmfId, subGroupMajor, gene) %>%
    # Determine optimal label
    dplyr::summarise(
        labelType = ifelse(any(grepl('Yes', onLabelNENSubtype)), 'On-label - NEN-subtype',
                           ifelse(any(grepl('Yes', onLabelNEN)), 'On-label - NEN',
                                  ifelse(any(grepl('Yes', offLabelClinicalPractice)), 'Off-label - NEN',
                                         ifelse(any(grepl('Yes', offLabelAvailable)), 'Off-label -  Other Cancer Types',
                                                ifelse(any(grepl('No', offLabelAvailable)), 'Drugs In Development', NA)))))
    ) %>%
    dplyr::mutate(labelType = factor(labelType, levels = c('On-label - NEN-subtype', 'On-label - NEN', 'Off-label - NEN', 'Off-label -  Other Cancer Types', 'Drugs In Development'))) %>%
    dplyr::ungroup()

# Sort on mut. excl.
memoData <- reshape2::dcast(heatData, gene~hmfId, value.var = 'labelType', fun.aggregate = function(x) ifelse(any(!is.na(x)), 1, 0))
rownames(memoData) <- memoData$gene; memoData$gene <- NULL
memoData[is.na(memoData)] <- 0
memoData <- memoSort(memoData)

heatData$hmfId <- factor(heatData$hmfId, levels = colnames(memoData))
heatData$gene <- factor(heatData$gene, levels = rev(rownames(memoData)))

plotB <- ggplot(heatData %>% tidyr::complete(hmfId, gene), aes(x = hmfId, y = gene, fill = labelType)) +
    geom_tile(color = 'grey80') +
    scale_fill_manual(values = c('On-label - NEN-subtype' = '#55C23B', 'On-label - NEN' = '#1F92AE', 'Off-label - NEN' = '#DC3F65', 'Off-label -  Other Cancer Types' = '#F49700', 'Drugs In Development' = '#9BEBEE'), guide = guide_legend(title = 'Types', title.position = 'top', title.hjust = 0.5, nrow = 1, keywidth = 0.75, keyheight = 0.75)) +
    labs(x = 'mNEN patients', y = 'Actionable alterations\n(Responsive)') +
    theme(
        text = element_text(family = 'Helvetica'),
        legend.position = 'none',
        panel.background = element_rect(fill = 'white', colour = 'white'),
        axis.title = element_text(face = 'bold',size = rel(1)),
        axis.title.y = element_text(angle = 90,vjust =2),
        axis.title.x = element_text(vjust = -0.2),
        axis.text.x = element_text(angle = 90, hjust = 1, vjust = 1, size = 6.5),
        axis.line = element_line(colour='black')
    ) + coord_equal()

# Overview of mutant samples per gene.
plotC <- data %>%
    dplyr::filter(grepl('Yes', onLabelNEN) | grepl('Yes', onLabelNENSubtype) | grepl('Yes', offLabelClinicalPractice) | grepl('Yes|No', offLabelAvailable)) %>%
    dplyr::filter(response == 'Responsive') %>%
    dplyr::group_by(hmfId, gene) %>%
    # Determine optimal label
    dplyr::summarise(
        labelType = ifelse(any(grepl('Yes', onLabelNENSubtype)), 'On-label - NEN-subtype',
                           ifelse(any(grepl('Yes', onLabelNEN)), 'On-label - NEN',
                                  ifelse(any(grepl('Yes', offLabelClinicalPractice)), 'Off-label - NEN',
                                         ifelse(any(grepl('Yes', offLabelAvailable)), 'Off-label -  Other Cancer Types',
                                                ifelse(any(grepl('No', offLabelAvailable)), 'Drugs In Development', NA)))))
    ) %>%
    dplyr::group_by(gene, labelType) %>% dplyr::summarise(n = length(unique(hmfId))) %>%
    dplyr::group_by(gene) %>% dplyr::mutate(totalN = sum(n)) %>%
    dplyr::mutate(labelType = factor(labelType, levels = c('On-label - NEN-subtype', 'On-label - NEN', 'Off-label - NEN', 'Off-label -  Other Cancer Types', 'Drugs In Development'))) %>%
    dplyr::ungroup() %>%
    dplyr::mutate(gene = factor(gene, levels = levels(heatData$gene))) %>%
    ggplot(., aes(x = n, y = gene, fill = labelType)) +
    geom_bar(stat = 'identity', color = 'black', width = 0.9) +
    scale_x_continuous(limits = c(0, 15), expand = expand_scale(mult = c(0, .05))) +
    geom_text(aes(label = sprintf('%s (%s%%)', stat(x), round(stat(x) / 86 * 100)), group = gene), stat = 'summary', fun = sum, vjust = .5, hjust = -.5, size = 2.5) +
    scale_fill_manual(values = c('On-label - NEN-subtype' = '#55C23B', 'On-label - NEN' = '#1F92AE', 'Off-label - NEN' = '#DC3F65', 'Off-label -  Other Cancer Types' = '#F49700', 'Drugs In Development' = '#9BEBEE'), guide = guide_legend(title = 'Types', title.position = 'top', title.hjust = 0.5, nrow = 1, keywidth = 0.75, keyheight = 0.75)) +
    labs(y = NULL, x = NULL) +
    theme(
        text = element_text(family = 'Helvetica'),
        legend.position = 'bottom',
        panel.background = element_rect(fill = 'white', colour = 'white'),
        axis.title = element_text(face = 'bold',size = rel(1)),
        axis.text.y = element_blank(),
        axis.title.y = element_blank(),
        axis.title.x = element_text(vjust = -0.2),
        axis.line = element_line(colour='black'),
        panel.grid.major.x = element_line(colour = 'grey80', linetype = 'dotted'),
        panel.grid.major.y = element_line(colour = 'grey80', linetype = 'dotted'),
        panel.grid.minor.y = element_line(colour = 'grey80', linetype = 'dotted')
    )


onco.biopsy <- ggplot(heatData, aes(hmfId, y = 'Subgroup', fill = subGroupMajor)) + geom_tile(colour = 'grey80', size = 0.5, na.rm = T) + labs(y = NULL, x = NULL) + annotationTheme() + theme(legend.position = 'bottom') +
    scale_fill_manual(values = c('mNEC' = '#C03830', 'mNET - Other' = '#D68A4F', 'mNET - Midgut' = '#1F79A3', 'mNET - Pancreas' = '#5B9173', 'mNET - Unknown' = '#454545',  'mNET - Lung' = '#D55957', 'mNET - Hindgut' = '#D68A4F', 'mNET - Foregut' = '#C5C5C5'), guide = guide_legend(title = 'Subgroup', title.position = 'top', title.hjust = 0.5, nrow = 1, keywidth = 0.75, keyheight = 0.75))

cowplot::plot_grid(plotA, plotB, plotC, ggplot(), onco.biopsy, ggplot(), align = 'hv', axis = 'tblr', nrow = 2, ncol = 3, rel_heights = c(.5, .2), rel_widths = c(.45, 1,.3))


# IClusion per sample -----------------------------------------------------

heatData <- data %>%
    dplyr::filter(source == 'iClusion') %>%
    dplyr::filter(grepl('Yes', onLabelNEN) | grepl('Yes', onLabelNENSubtype) | grepl('Yes', offLabelClinicalPractice) | grepl('Yes', offLabelAvailable)) %>%
    dplyr::filter(response == 'Responsive') %>%
    dplyr::group_by(gene) %>%
    dplyr::mutate(type = ifelse(type == 'radiotherapy', 'Radiotherapy', type)) %>%
    dplyr::mutate(type = ifelse(type == 'Trial', 'Eligible for assorted trials', type)) %>%
    dplyr::mutate(trials = ifelse(length(unique(type)) < 10, paste(unique(type), collapse = ', '), '≥10 trials')) %>%
    dplyr::ungroup() %>%
    dplyr::group_by(trials) %>%
    dplyr::mutate(genes = ifelse(length(unique(gene)) < 10, paste(unique(gene), collapse = ', '), '≥10 gene')) %>%
    dplyr::mutate(id = sprintf('%s - %s', trials, genes)) %>%
    dplyr::ungroup() %>%
    tidyr::complete(hmfId, id) %>%
    dplyr::mutate(value = ifelse(!is.na(genes), 'Yes', NA))

# Sort on mut. excl.
memoData <- reshape2::dcast(heatData, id~hmfId, value.var = 'value', fun.aggregate = function(x) ifelse(any(!is.na(x)), 1, 0))
rownames(memoData) <- memoData$id; memoData$id <- NULL
memoData[is.na(memoData)] <- 0
memoData <- memoSort(memoData)

heatData$hmfId <- factor(heatData$hmfId, levels = colnames(memoData))
heatData$id <- factor(heatData$id, levels = rev(rownames(memoData)))
heatData$value <- factor(heatData$value)

plotD <- heatData %>%
    ggplot(., aes(x = hmfId, y = id, fill = value)) +
    geom_tile(color = 'grey80') +
    scale_fill_manual(values = c('Yes' = '#DF1068', 'NA' = 'white'), guide = guide_legend(title = 'Eligibility', title.position = 'top', title.hjust = 0.5, nrow = 1, keywidth = 0.75, keyheight = 0.75)) +
    labs(x = 'mNEN patients', y = 'Eligible therapies / trials') +
    theme(
        text = element_text(family = 'Helvetica', size = 9),
        legend.position = 'none',
        panel.background = element_rect(fill = 'white', colour = 'white'),
        axis.title = element_text(face = 'bold',size = rel(1)),
        axis.title.y = element_text(angle=90,vjust =2),
        axis.title.x = element_text(vjust = -0.2),
        axis.text.x = element_text(angle = 90, hjust = 1, vjust = 1, size = 6.5),
        axis.line = element_line(colour='black') + coord_equal()
    )

# Plot biopsy location
onco.biopsy <- ggplot(heatData, aes(hmfId, y = 'Subgroup', fill = subGroupMajor)) + geom_tile(colour = 'grey80', size = 0.5, na.rm = T) + labs(y = NULL, x = NULL) + annotationTheme() + theme(legend.position = 'bottom') +
    scale_fill_manual(values = c('mNEC' = '#C03830', 'mNET - Other' = '#D68A4F', 'mNET - Midgut' = '#1F79A3', 'mNET - Pancreas' = '#5B9173', 'mNET - Unknown' = '#454545',  'mNET - Lung' = '#D55957', 'mNET - Hindgut' = '#D68A4F', 'mNET - Foregut' = '#C5C5C5'), guide = guide_legend(title = 'Subgroup', title.position = 'top', title.hjust = 0.5, nrow = 1, keywidth = 0.75, keyheight = 0.75))

cowplot::plot_grid(plotD, onco.biopsy, align = 'hv', axis = 'tblr', nrow = 2, rel_heights = c(.5, .3))


# Write to table ----------------------------------------------------------

z <- data %>%
    dplyr::filter(grepl('Yes', onLabelNEN) | grepl('Yes', onLabelNENSubtype) | grepl('Yes', offLabelClinicalPractice) | grepl('Yes|No', offLabelAvailable)) %>%
    dplyr::mutate(
        labelType = ifelse(grepl('Yes', onLabelNENSubtype), 'On-label - NEN-subtype',
                           ifelse(grepl('Yes', onLabelNEN), 'On-label - NEN',
                                  ifelse(grepl('Yes', offLabelClinicalPractice), 'Off-label - NEN',
                                         ifelse(grepl('Yes', offLabelAvailable), 'Off-label -  Other Cancer Types',
                                                ifelse(grepl('No', offLabelAvailable), 'Drugs In Development', NA)))))
    )

write.table(z, file = 'asd.txt', quote = F, row.names = F, sep = '\t')


# Summary on Subgroups ----------------------------------------------------

z <- data %>%
    dplyr::filter(grepl('Yes', onLabelNEN) | grepl('Yes', onLabelNENSubtype) | grepl('Yes', offLabelClinicalPractice) | grepl('Yes|No', offLabelAvailable)) %>%
    dplyr::filter(response == 'Responsive') %>%
    dplyr::distinct(sampleId, subGroupMajor, gene)

table(z$gene, z$subGroupMajor) %>% View
