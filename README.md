# Analysis of the advanced neuro-endocrine cohort (CPCT-02; DR-036)

This repository contains the custom R scripts / workflow used to perform the WGS analysis of the aNEN samples of the CPCT-02 consortium (*n* = 85). This workflow performed on the aNEN cohort is further detailed by Riet et al. in Nature Communications (2021): [The genomic landscape of 85 advanced neuroendocrine neoplasms reveals subtype-heterogeneity and potential therapeutic targets](https://www.google.com/).
This workflow is dependent on processed HMF data which are used to generate all the assorted figures and respective data as incorporated within the manuscript.
See the data-availabity section on how to acquire the initial data from the Hartwig Medical Foundation (HMF).

This workflow (in *R*) was performed on Ubuntu 18.04.5 LTS with the following *R* version:
```R
R version 3.6.2 (2019-12-12) -- "Dark and Stormy Night"
Copyright (C) 2019 The R Foundation for Statistical Computing
Platform: x86_64-pc-linux-gnu (64-bit)
```

This workflow is rather RAM-heavy and ideally requires ~100-200GB of RAM to be performed in parallel due to the large size of WGS input files (VCF, PURPLE and GRIDSS) and corresponding annotations. Due to the low mutational burden of aNEN, this can be performed with ~50GB. Importing data can be parallized by specifying the `ncores = XX` argument.
The majority of the time is spent on importing, converting and summarizing the HMF data (`1.importData.R`). The total processing time is dependent on the number of samples, for aNEN this can take up to 12 hours.

**Additional required software modules.**

Several additional software suites are required to run this workflow:

*  [R2CCBC (v0.8.1)](https://bitbucket.org/ccbc/r2ccbc/)
    * Commit used in the aNEN analysis: [e9dc0](https://bitbucket.org/ccbc/r2ccbc/commits/e9dc0db2d163ccf5e273f41425f47c82de5d77c6).
    
*  [Variant Effect Predictor (release v99; hg19)](https://www.ensembl.org/info/docs/tools/vep/script/vep_download.html)
    * Install instruction and annotation [scripts](https://bitbucket.org/ccbc/ccbc_annotation/src/master/) (as used within the CCBC).
    * Downloading and installing the VEP cache can take several hours.
    
*  [GISTIC2 (v2.0.23 with hg19.mat).](https://portals.broadinstitute.org/cgi-bin/cancer/publications/view/216/)

*  [pacman](https://cran.r-project.org/web/packages/pacman/index.html)
    * Additional R packages are installed via *pacman*.

## Running the workflow.

The raw and processed data of this cohort can be requested under the data-request **DR-036** from the Hartwig Medical Foundation (HMF): https://www.hartwigmedicalfoundation.nl/en/applying-for-data/. The input data which annotated samples with clinical trial or drug eligibility based on somatic aberrations was requested from [iClusion](https://www.iclusion.com/) and were supplemented with extra records detailing the samples with TMB > 10 (checkpoint inhibitors).

After installation of `R2CCBC` on your local machine, the scripts can be performed in the order listed in the *R/* folder to generate all the respective data and corresponding figures; starting with `1.importData.R` which imports the various input files and processes them into combined reports and various RData object for use in the later scripts.

For ease, we generate symlinks of all required input data into a single folder (*combinedData/*). As part of the first step, the VCF files have to be further annotated by a custom `VEP` command, the corresponding [scripts](https://bitbucket.org/ccbc/ccbc_annotation/src/master/) to generate the correct GTF (used in combination with the VEP cache; hg19, v99) have also been made available.

Throughout the scripts, the following paths need to be altered by the user:

* Change */mnt/data2/hartwig/DR36/v2/* to the main input (and output) folder of your choice which holds the HMF and processed data.
